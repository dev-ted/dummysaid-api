var saIdParser = require("south-african-id-parser");
var { isNil, isEmpty } = require("ramda");

var moment = require("moment");

const exists = (i) => !isNil(i) && !isEmpty(i);

var randomValueBetween = function randomValueBetween(min, max) {
  return Math.random() * (max - min) + min;
};

var randomDate = function randomDate(d1, d2) {
  var date1 = d1 || "01-01-1970";
  var date2 = d2 || new Date();
  date1 = new Date(date1).getTime();
  date2 = new Date(date2).getTime();
  return date1 > date2
    ? new Date(randomValueBetween(date2, date1))
    : new Date(randomValueBetween(date1, date2));
};

var getRandomSexDigits = function getRandomSexDigits() {
  return Math.floor(1000 + Math.random() * 9000);
};

var getFormatedSexDigits = function (genderStart, genderEnd) {
  const randomThreeNumbers = Math.floor(100 + Math.random() * 900);
  const genderDigit = Math.round(randomValueBetween(genderStart, genderEnd));
  return "".concat(genderDigit).concat(randomThreeNumbers);
};

var getFormatedRandomDate = function getFormatedRandomDate(age) {
  var currentYear = new Date().getFullYear();
  var ageBirthDate = moment(new Date())
    .subtract(parseInt(age) + 1, "years")
    .add(1, "days");
  const yearAge = currentYear - age;
  var ageYrEnd = "".concat(yearAge, "/12/31");

  var randomdate = randomDate(ageBirthDate.format("YYYY/MM/DD"), ageYrEnd);
  return moment(randomdate).format("YYMMDD");
};

var getRandomCitizenshipDigit = function getRandomCitizenshipDigit() {
  return randomValueBetween(0, 1);
};

var getRandomZdigit = function getRandomZdigit() {
  return randomValueBetween(80, 89);
};

var _generateFakeId = function _generateFakeId(age) {
  var date = getFormatedRandomDate(age);
  var sex = getRandomSexDigits();
  var citizenship = Math.round(getRandomCitizenshipDigit());
  var zDigit = Math.round(getRandomZdigit());
  return "".concat(date).concat(sex).concat(citizenship).concat(zDigit);
};

var _generateFakeIdByGender = function _generateFakeId(gender) {
  var age = Math.floor(Math.random() * 90 + 10);
  var date = getFormatedRandomDate(age);
  const genderRangeStart = gender === "M" ? 5 : 0;
  const genderRangeEnd = gender === "M" ? 9 : 4;
  var sex = getFormatedSexDigits(genderRangeStart, genderRangeEnd);
  var citizenship = Math.round(getRandomCitizenshipDigit());
  var zDigit = Math.round(getRandomZdigit());
  return "".concat(date).concat(sex).concat(citizenship).concat(zDigit);
};

var _generateFakeIdByAgeAndGender = function _generateFakeId({ gender, age }) {
  var date = getFormatedRandomDate(age);
  const genderRangeStart = gender === "M" ? 5 : 0;
  const genderRangeEnd = gender === "M" ? 9 : 4;
  var sex = getFormatedSexDigits(genderRangeStart, genderRangeEnd);
  var citizenship = Math.round(getRandomCitizenshipDigit());
  var zDigit = Math.round(getRandomZdigit());
  return "".concat(date).concat(sex).concat(citizenship).concat(zDigit);
};

////////////////////////////////////////////////////////////////////////////////////
// VALID ID GENERATION

var generateFakeIdByAge = function generateFakeIdByAge(age) {
  var fakeId = _generateFakeId(age);
  var isValid = saIdParser.validate(fakeId);
  return isValid ? fakeId : generateFakeIdByAge(age);
};

var generateFakeIdByGender = function (gender) {
  var fakeId = _generateFakeIdByGender(gender);
  var isValid = saIdParser.validate(fakeId);
  return isValid ? fakeId : _generateFakeIdByGender(gender);
};

var generateFakeIdByAgeAndGender = function ({ age, gender }) {
  var fakeId = _generateFakeIdByAgeAndGender({ age, gender });
  var isValid = saIdParser.validate(fakeId);
  return isValid === true
    ? fakeId
    : generateFakeIdByAgeAndGender({ age, gender });
};

////////////////////////////////////////////////////////////
// INVALID ID GENERATION

var generateInvalidFakeIdByAge = function (age) {
  var fakeId = _generateFakeId(age);
  var isValid = saIdParser.validate(fakeId);
  return !isValid ? fakeId : generateInvalidFakeIdByAge(age);
};

var generateInvalidFakeIdByGender = function (gender) {
  var fakeId = _generateFakeIdByGender(gender);

  var isValid = saIdParser.validate(fakeId);
  return !isValid ? fakeId : _generateFakeIdByGender(gender);
};

var generateInvalidFakeIdByAgeAndGender = function ({ age, gender }) {
  var fakeId = _generateFakeIdByAgeAndGender({ age, gender });
  var isValid = saIdParser.validate(fakeId);
  return !isValid
    ? fakeId
    : generateInvalidFakeIdByAgeAndGender({ age, gender });
};

////////////////////////////////////////////////////////////////////////////////////
// EXPORTS

exports.generateFakeId = function (data) {
  if (exists(data)) {
    const { gender, age } = data;
    if (
      gender &&
      gender.toUpperCase() !== "M" &&
      gender.toUpperCase() !== "F"
    ) {
      return {
        errors: [
          "Invalid gender specified ( " + gender + " ) Please use M or F",
        ],
      };
    }
    if (age && !gender) {
      return generateFakeIdByAge(age);
    }
    if (gender && !age) {
      return generateFakeIdByGender(gender);
    }
    if (age && gender) {
      return generateFakeIdByAgeAndGender({ gender, age });
    }
  }

  var age = Math.floor(Math.random() * 90 + 10);
  return generateFakeIdByAge(age);
};

exports.generateInvalidFakeId = function (data) {
  if (exists(data)) {
    const { gender, age } = data;
    if (
      gender &&
      gender.toUpperCase() !== "M" &&
      gender.toUpperCase() !== "F"
    ) {
      return {
        errors: [
          "Invalid gender specified ( " + gender + " ) Please use M or F",
        ],
      };
    }
    if (age && !gender) {
      return generateInvalidFakeIdByAge(age);
    }
    if (gender && !age) {
      return generateInvalidFakeIdByGender(gender);
    }
    if (age && gender) {
      return generateInvalidFakeIdByAgeAndGender({ gender, age });
    }
  }

  var age = Math.floor(Math.random() * 90 + 10);
  return generateInvalidFakeIdByAge(age);
};

exports.isValid = function (id) {
  return saIdParser.validate(id);
};
