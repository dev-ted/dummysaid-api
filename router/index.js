const express = require("express");
const { validate } = require('../lib/validate');
const util = require('../lib/id');

const router = express.Router();

function* range(start, end) {
    yield start;
    if (start === end) return;
    yield* range(start + 1, end);
}


// BATCHES

router.post("/batch/validate/specific", async function (req, res) {
  const body = req.body;
  const { collection } = body;
  if(!Array.isArray(collection)){
      res.status(400).send({error: "Incorrect collection type provided, Excpected array"})
  }
  else if(collection.length === 0){
    res.status(400).send({error: "Collection is empty."})
  }
  else{
    const validations = collection.map(item => {
        return validate(item)
    })
    const validIds = validations.filter(v => v.id.isValid === true);
    const invalidIds = validations.filter(v => !v.id.isValid === true);
    const idsWithIncorrectConfig = validations.filter(v => {
        const properties = Object.keys(v);
        const invalidProperties = properties.find(p => !v[p].isValid)
        return invalidProperties !== undefined;
    });
    res.status(200).send({ 
        idsReceived: collection.length,
        idsValidated: validations.length,
        validIds: {
            count: validIds.length,            
            data: validIds,
        },
        idsWithIncorrectConfig: {
            count: idsWithIncorrectConfig.length,
            data: idsWithIncorrectConfig
        },
        invalidIds: {
            count: invalidIds.length,
            data: invalidIds
        },
        completeValidation: validations, 
    })
  }
});

router.post("/batch/validate", async function (req, res) {
    const body = req.body;
    const { collection } = body;
    if(!Array.isArray(collection)){
        res.status(400).send({error: "Incorrect collection type provided, Excpected array"})
    }
    else if(collection.length === 0){
        res.status(400).send({error: "Collection is empty."})
    }
    else{
        const validations = collection.map(id => {
            return { id, isValid: util.isValid(id)}
        })
        const validIds = validations.filter(v => v.isValid === true);
        const invalidIds = validations.filter(v => !v.isValid === true);
        res.status(200).send({
            idsReceived: collection.length,
            idsValidated: validations.length,
            validIds: {
                count: validIds.length,            
                data: validIds,
            },
            invalidIds: {
                count: invalidIds.length,
                data: invalidIds
            }
        })
    }
});

router.post("/batch/generate/valid/dummy_id", async function (req, res) {
    const body = req.body;
    const { limit } = body;
    if(!limit || isNaN(limit)){
        res.status(400).send({error: "Incorrect limit received", limit: `${limit}`})
    }
    const collection = [...range(0,limit-1)].map(i =>  util.generateFakeId())
    res.status(200).send({ collection })
});

router.post("/batch/generate/specific/valid/dummy_id", async function (req, res) {
    const body = req.body;
    const { gender, age, limit } = body;
    if(!limit || isNaN(limit) || parseInt(limit) === 0){
        res.status(400).send({error: "Incorrect limit received", limit: `${limit}`})
    }
    const collection = [...range(0,parseInt(limit)-1)].map(i =>  ({gender, age, id: util.generateFakeId({gender, age})}))
    res.status(200).send({ collection })
});

router.post("/batch/generate/specific/valid/dummy_id/collectively", async function (req, res) {
    const body = req.body;
    const { collection } = body;
    if(!Array.isArray(collection)){
        res.status(400).send({error: "Incorrect collection type provided, Excpected array", collection})
    }
    else if(collection.length === 0){
        res.status(400).send({error: "Collection is empty.", collection})
    }
    const newCollection = collection.map(i =>  {
        const { gender, age } = i;
        return ({
            id:util.generateFakeId(i),
            gender: `${gender}`,
            age: `${age}`
        })
    })
    res.status(200).send({ collection: newCollection })
});

router.post("/batch/generate/invalid/dummy_id", async function (req, res) {
    const body = req.body;
    const { limit } = body;
    if(!limit || isNaN(limit)){
        res.status(400).send({error: "Incorrect limit received", limit: `${limit}`})
    }
    const collection = [...range(0,limit-1)].map(i =>  util.generateInvalidFakeId())
    res.status(200).send({ collection })
});


router.post("/batch/generate/specific/invalid/dummy_id", async function (req, res) {
    const body = req.body;
    const { gender, age, limit } = body;
    if(!limit || isNaN(limit) || parseInt(limit) === 0){
        res.status(400).send({error: "Incorrect limit received", limit: `${limit}`})
    }
    const collection = [...range(0,parseInt(limit)-1)].map(i =>  ({gender, age, id: util.generateInvalidFakeId({gender, age})}))
    res.status(200).send({ collection })
});

router.post("/batch/generate/specific/invalid/dummy_id/collectively", async function (req, res) {
    const body = req.body;
    const { collection } = body;
    if(!Array.isArray(collection)){
        res.status(400).send({error: "Incorrect collection type provided, Excpected array", collection})
    }
    else if(collection.length === 0){
        res.status(400).send({error: "Collection is empty.", collection})
    }
    const newCollection = collection.map(i =>  {
        const { gender, age } = i;
        return ({
            id:util.generateInvalidFakeId(i),
            gender: `${gender}`,
            age: `${age}`
        })
    })
    res.status(200).send({ collection: newCollection })
});




// SINGLES


router.post("/validate/specific", async function (req, res) {
    const body = req.body;
    const { id, gender, age } = body;
    res.status(200).send(validate({id, gender, age}))
});

router.post("/validate", async function (req, res) {
    const body = req.body;
    const { id } = body;
    const isValid = util.isValid(id);
    res.status(200).send({id, isValid})
});

router.get("/generate/valid/dummy_id", async function (req, res) {
    res.status(200).send({id: util.generateFakeId()})
});

router.post("/generate/specific/valid/dummy_id", async function (req, res) {
    const body = req.body;
    const { gender, age } = body;
    res.status(200).send({id: util.generateFakeId({gender, age})})
});

router.get("/generate/invalid/dummy_id", async function (req, res) {
    res.status(200).send({id: util.generateInvalidFakeId()})
});

router.post("/generate/specific/invalid/dummy_id/", async function (req, res) {
    const body = req.body;
    const { gender, age } = body;
    res.status(200).send({id: util.generateInvalidFakeId({gender, age})})
});

module.exports = router;
