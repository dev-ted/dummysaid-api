const express = require("express");
const cors = require("cors");
const Rollbar = require('rollbar')
const useragent = require('express-useragent');
const requestIp = require('request-ip');
const apiRoutes = require("./router");
const bodyParser = require("body-parser");
require('dotenv').config()

console.log("************************************")
console.log("** Setup Rollbar")

var rollbar = new Rollbar({
  accessToken: process.env.ROLLBAR_TOKEN,
  captureUncaught: true,
  captureUnhandledRejections: true
});


console.log("** Setup Express server")
let app = express();
var PORT = process.env.PORT || 3000;
console.log(`** PORT ${PORT}`)

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(cors());
app.use(useragent.express());
app.use(requestIp.mw())
app.use(rollbar.errorHandler());
;

app.get("/", (req, res) => res.redirect('http://saiddocs.touch.net.za'));

app.use("/v1/", apiRoutes);


rollbar.log('Server Started ', new Date());

app.listen(PORT, () => {
    console.log(`** Server is listening on port ${PORT}`);
  });

